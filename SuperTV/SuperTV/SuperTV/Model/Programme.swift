//
//  Programme.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import Foundation

struct Programme {
    
    public private(set) var title:String!
    public private(set) var end_date:String!
    public private(set) var start_date:String!
    
    init(){}
    
    init(title:String,end_date:String,start_date:String) {
        self.title = title
        self.end_date = end_date
        self.start_date = start_date
    }
}
