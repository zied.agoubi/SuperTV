//
//  Channel.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import Foundation

struct Channel {
    
    public private(set) var id:Int!
    public private(set) var title:String!
    public private(set) var programme:[Programme]!
    
    
    init(){}
    
    init(id:Int,title:String,programme:[Programme]) {
        self.id = id
        self.title = title
        self.programme = programme
    }
    
    init(id:Int,title:String) {
        self.id = id
        self.title = title
    }
}
