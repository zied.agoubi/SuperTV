//
//  ChannelServices.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class ChannelServices {
    
    static let instance = ChannelServices()
    
    var channelsList =  [
        Channel(id: 1, title: "CHANNEL 1"),
        Channel(id: 2, title: "CHANNEL 2"),
        Channel(id: 3, title: "CHANNEL 3"),
        Channel(id: 4, title: "CHANNEL 4"),
        Channel(id: 5, title: "CHANNEL 5"),
        Channel(id: 6, title: "CHANNEL 6"),
        Channel(id: 7, title: "CHANNEL 7"),
    ]
    
    var AllChannels = [Channel]()
    var Channels = [Channel]()
    var AllPrograms = [Programme]()
    var channel:Channel!
    var prog:Programme!
    var message:String = ""
    
    
    //fonction get all product
    func findAllChannelsMethod(theUrl:String,completion: @escaping CompletionHandler) {
        let url = URL(string: theUrl)
        URLSession.shared.dataTask(with: url!){(data,response, error) in
            if error != nil {
                print("didnt work")

            } else {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                    
                    for (key,value) in parsedData {
                      if key == "channels" {
                        if let channelsArray: [[String:Any]] = value as? [[String:Any]] {
                            print ("is a dictionary")
                            for dict in channelsArray{
                                
                                for (key,value) in dict {
                                    var idch:Int = 0
                                    var titch:String = ""
                                    if (key == "id"){
                                        print(value)
                                        idch = value as! Int
                                    }else if (key == "title"){
                                        print(value)
                                        titch = value as! String
                                    }else if (key == "programme"){
                                        //***************
                                        
                                            if let programmeArray: [[String:Any]] = value as? [[String:Any]] {
                                                print ("is a dictionary")
                                                for dic in programmeArray{
                                                    
                                                    var tit:String = ""
                                                    var enddt:String = ""
                                                    var startdt:String = ""
                                                    for (key,value) in dic {
                                                        if (key == "title"){
                                                            print(value)
                                                            tit = value as! String
                                                        }else if (key == "end_date"){
                                                            print(value)
                                                            enddt = value as! String
                                                        }else if (key == "start_date"){
                                                            print(value)
                                                            startdt = value as! String
                                                        }
                                                        let prg = Programme(title: tit, end_date: enddt, start_date: startdt)
                                                        self.AllPrograms.append(prg)
                                                        
                                                    }//end for
                                                    
                                                }
                                            }
                                        
                                        //***************
                                    }
                                    let chan = Channel(id: idch, title: titch, programme: self.AllPrograms)
                                    self.AllChannels.append(chan)
                                    for it in self.AllChannels {
                                        self.Channels.append(it)
                                    }
                                    
                                }//end for
                                
                            }
                        }
                        
                     }//end if
                    
                    }//end for
                
                }catch let error as NSError{
                    print(error)
                }
            }
            
        }.resume()
    }
    
}











