//
//  ViewController.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import UIKit

class ChannelController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var channelTableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        channelTableView.dataSource = self
        channelTableView.delegate = self
        
        ChannelServices.instance.findAllChannelsMethod(theUrl: "https://gist.githubusercontent.com/reden87/ad856e7994b8ea93ac27503ecb051347/raw/050c539749f3d253a01ad685983ebc8503ea7872/example.json") { (sucess) in
            if sucess == true {
                for it in ChannelServices.instance.Channels {
                    
                    print(it.title)
                    
                }
            }
        }
        
        
       
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChannelServices.instance.channelsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "channelCell") as? ChannelCell{
            
            let channel = ChannelServices.instance.channelsList[indexPath.row]
            cell.updatesViewsChannel(channel: channel)
            
            return cell
        } else {
            return ChannelCell()
        }
    }
    
}

