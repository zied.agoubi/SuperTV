//
//  PrgramsCell.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import UIKit

class PrgramsCell: UITableViewCell {

    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var nameProgram: UILabel!
    @IBOutlet weak var endTime: UILabel!
    
    
    func updatesViewsProgram(programme:Programme){
        startTime.text = programme.start_date
        nameProgram.text = programme.title
        endTime.text = programme.end_date
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
