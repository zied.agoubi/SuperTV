//
//  SlpashScreenController.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import UIKit

class SlpashScreenController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ChannelServices.instance.findAllChannelsMethod { (success) in
            
        }
        
        Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(dismissSplashController), userInfo: nil, repeats: false)
    
    }
    
    @objc func dismissSplashController() {
        performSegue(withIdentifier: "goM", sender: nil)
    }
    
}
