//
//  ProgramController.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import UIKit

class ProgramController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var lstPrograms: UITableView!
    var AllProgs = [Programme]()
    
    override func viewDidAppear(_ animated: Bool) {
       //print("its the \(ch.programme!)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lstPrograms.dataSource = self
        lstPrograms.delegate = self
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AllProgs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "pCell") as? PrgramsCell{
            
            let programme = AllProgs[indexPath.row]
            cell.updatesViewsProgram(programme: programme)
            
            return cell
        } else {
            return PrgramsCell()
        }
    }

}
