//
//  ViewController.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import UIKit

class ChannelController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var channelTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        channelTableView.dataSource = self
        channelTableView.delegate = self
         
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChannelServices.instance.AllChannel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "channelCell") as? ChannelCell{
            let channel = ChannelServices.instance.AllChannel[indexPath.row]
            cell.updatesViewsChannel(channel: channel)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let programme = ChannelServices.instance.AllChannel[indexPath.row].programme
        performSegue(withIdentifier: "goP", sender: programme)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let pVC = segue.destination as? ProgramController {
            let barBtn = UIBarButtonItem()
            barBtn.title = ""
            navigationItem.backBarButtonItem = barBtn
            
            assert(sender as? [Programme] != nil)
            pVC.AllProgs = sender as! [Programme]
        }
        
    }
}

