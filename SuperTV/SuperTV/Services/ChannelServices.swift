//
//  ChannelServices.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class ChannelServices {
    
    static let instance = ChannelServices()
    
    var AllChannels = [Channel]()
    
    
    //var AllChan = [Channel]()
    var AllPrgm = [Programme]()
    
    let AllChannel = [
        Channel(id: "1", title: "CHANNEL 1", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
        ]),
        Channel(id: "1", title: "CHANNEL 2", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "1", title: "CHANNEL 3", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "1", title: "CHANNEL 4", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "5", title: "CHANNEL 5", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "6", title: "CHANNEL 6", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "7", title: "CHANNEL 7", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "8", title: "CHANNEL 8", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "9", title: "CHANNEL 9", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "10", title: "CHANNEL 10", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "11", title: "CHANNEL 11", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
        Channel(id: "12", title: "CHANNEL 12", programme: [
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            Programme(title: "Ab Dolorem Sapiente Et", end_date: "2017-01-15T23:46:50Z", start_date: "2017-01-15T23:00:00Z"),
            ]),
    ]
    
    /*func findAllChannelsMethod(theUrl:String,completion: @escaping CompletionHandler) {
        
        let url = URL(string: theUrl)
        URLSession.shared.dataTask(with: url!){(data,response, error) in
            if error != nil {
                print("didnt work")

            } else {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data!) as! [String:Any]
                    
                    for (key,value) in parsedData {
                      if key == "channels" {
                        if let channelsArray: [[String:Any]] = value as? [[String:Any]] {
                            for dict in channelsArray{
                                
                                for (key,value) in dict {
                                    var idch:String = ""
                                    var titch:String = ""
                                    var AllPrograms = [Programme]()
                                    if (key == "id"){
                                        idch = "\(value)"
                                    }else if (key == "title"){
                                        titch = value as! String
                                    }else if (key == "programme"){
                                            if let programmeArray: [[String:Any]] = value as? [[String:Any]] {
                                                for dic in programmeArray{
                                                    var tit:String = ""
                                                    var enddt:String = ""
                                                    var startdt:String = ""
                                                    for (key,value) in dic {
                                                        if (key == "title"){
                                                            tit = value as! String
                                                        }else if (key == "end_date"){
                                                            enddt = value as! String
                                                        }else if (key == "start_date"){
                                                            startdt = value as! String
                                                        }
                                                    }
                                                    let p = Programme(title: tit, end_date: enddt, start_date: startdt)
                                                    AllPrograms.append(p)
                                                }
                                            }
                                    }
                                    let chan = Channel(id: idch, title: titch, programme: AllPrograms)
                                    self.AllChannels.append(chan)
                                    AllPrograms.removeAll()
                                }
                                
                            }
                        }
                        
                     }
                    
                    }
                
                }catch let error as NSError{
                    print(error)
                }
            }
            
        }.resume()
    }*/
    
    func findAllChannelsMethod(completion: @escaping CompletionHandler){
        AllChannels.removeAll()
        Alamofire.request(URL_CHANNELS, method: .get).responseJSON {
            (response) in
            
            if response.result.error == nil {
                guard let data = response.data else { return }
                if let json = JSON(data: data).dictionary{
                    for (key,value) in json {
                        if (key=="channels"){
                            print(value)
                        }
                        completion(true)
                    }
                }
                
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }//end alamofire
    }
    
}











