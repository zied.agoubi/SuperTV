//
//  Channel.swift
//  SuperTV
//
//  Created by zied agoubi on 6/30/18.
//  Copyright © 2018 zied agoubi. All rights reserved.
//

import Foundation

struct Channel {
    
    public var id:String!
    public var title:String!
    public var programme:[Programme]!
    
    
    init(){}
    
    init(id:String,title:String,programme:[Programme]) {
        self.id = id
        self.title = title
        self.programme = programme
    }
    
    init(id:String,title:String) {
        self.id = id
        self.title = title
    }
}
